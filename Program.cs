﻿using System;
/* CASO DE ESTUDIO #1 */
/* Empresa agroalimentaria|código generado por:  */
/* Jhoana Abigail Garcia Ancona */
/* Yeny Guadalupe Domínguez Yam */
/* Jonathan Alberto Loria Reyes */

namespace Caso1Agroaliment
{
   
class ProductoCongelado
{
    public String CodigoOSA { get; set; }
    public String FechaEnvasado { get; set; }
    public double TempRecomendada { get; set; }
    public String PaisOrigen { get; set; }

    public ProductoCongelado(String codigoOSA, String fechaEnvasado, double tempRecomendada, String paisOrigen)
    {
        CodigoOSA = codigoOSA;
        FechaEnvasado = fechaEnvasado;
        TempRecomendada = tempRecomendada;
        PaisOrigen = paisOrigen;
    }
}
class ProductoFresco
{
    public String FechaCaducidad { get; set; }
    public int NumeroLote { get; set; }
    public String FechaEnvasado { get; set; }
    public String PaisOrigen { get; set; }

    public ProductoFresco(String fechaCaducidad, int numeroLote, String fechaEnvasado, string paisOrigen)
    {
        FechaCaducidad = fechaCaducidad;
        NumeroLote = numeroLote;
        FechaEnvasado = fechaEnvasado;
        PaisOrigen = paisOrigen;

    }
}
class ProductoRefrigerado
{
    public String FechaEnvasado { get; set; }
    public String PaisOrigen { get; set; }
    public double TempRecomendada { get; set; }

    public ProductoRefrigerado(String fechaEnvasado, String paisOrigen, double tempRecomendada)
    {
        FechaEnvasado = fechaEnvasado;
        PaisOrigen = paisOrigen;
        TempRecomendada = tempRecomendada;
    }
}
    class CongeladoAire
    {
        public double Nitrogeno { get; set; }
        public double Oxigeno { get; set; }
        public double CO2 { get; set; }
        public double H2O { get; set; }

        /*public CongeladoAire(double nitrogeno, double oxigeno, double co2, double h2o)
        {
            Nitrogeno = nitrogeno;
            Oxigeno = oxigeno;
            CO2 = co2;
            H2O = h2o;
        }*/
    }
    class CongeladoNitrogeno
    {
        public String MetodoCongelacion { get; set; }
        public int TiempoExposicion { get; set; }

        public CongeladoNitrogeno(String metodoCongelacion, int tiempoExposicion)
        {
            MetodoCongelacion = metodoCongelacion;
            TiempoExposicion = tiempoExposicion;
        }
    }
    class CongeladosAgua
    {
        public double SalinidadAgua { get; set; }


    }
    class Program
{
    static void Main(string[] args)
    {

        ProductoFresco prodFresco1 = new ProductoFresco(" 06/06/2020", 001, "12/05/2019", "Mexico, CDmx");
        ProductoCongelado prodCong1 = new ProductoCongelado("002", "20/03/2019", 0.2, "Mexico, CDMX");
        ProductoRefrigerado prodRefrig1 = new ProductoRefrigerado("20/05/2019", "Mexico, CDMX", 15.00);
        CongeladoNitrogeno prodNitr1 = new CongeladoNitrogeno("Aire", 0);
        CongeladosAgua prodCongAgua1 = new CongeladosAgua();
            prodCongAgua1.SalinidadAgua = 64.00;

            CongeladoAire prodAire1 = new CongeladoAire();
            prodAire1.Nitrogeno = 22.00;
            prodAire1.Oxigeno = 52.00;
            prodAire1.H2O = 12.00;
            prodAire1.CO2 = 78.00;

        Console.WriteLine("**Producto confelado por Aire:**\nNITROGENO: "+ prodAire1.Nitrogeno+"%\nOXIGENO: "+prodAire1.Oxigeno+"%\nVAPOR DE AGUA: "+prodAire1.H2O+"%\nDIOXIDO DE CARBONO: "+ prodAire1.CO2+"% \n");
    

        Console.WriteLine("**Datos Producto fresco:** \nPais de origen: " + prodFresco1.PaisOrigen + " Fecha de Caducidad: " + prodFresco1.FechaCaducidad + " Fecha de envasado: " + prodFresco1.FechaEnvasado + " No. de lote: " + prodFresco1.NumeroLote+ "\n");
        Console.WriteLine("**Datos Primer producto refrigerado:** \nFecha de envasado: " + prodRefrig1.FechaEnvasado + " Pais de origen: " + prodRefrig1.PaisOrigen + " Tep. recomendada: " + prodRefrig1.TempRecomendada + "\n");
        Console.WriteLine("**Datos Primer producto congelado:** \nFecha de envasado: " + prodCong1.FechaEnvasado + " " + prodCong1.CodigoOSA + " " + prodCong1.PaisOrigen + " " + prodCong1.TempRecomendada + "\n");

        Console.WriteLine("**Datos Primer producto congelado con agua:** Salinidad de agua:" + prodCongAgua1.SalinidadAgua + "\n");
        Console.WriteLine("**Datos Primer producto congelado con nitrogeno:** \nMetodo de congelacion: " + prodNitr1.MetodoCongelacion + " Tiempo de exposicion: " + prodNitr1.TiempoExposicion + "\n");

    }
}
}
